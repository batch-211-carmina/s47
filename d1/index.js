console.log(document);
console.log(document.querySelector("#txt-first-name"));
console.log(document.querySelector("#txt-last-name"));

// result - element from HTML
/*
	document - refers to the whole webpage
	querySelector - used to select a specific element (object) as long as it is inside the html tag(HTML ELEMENT);
	- takes a string input that is formatted like CSS Selector
	- can select elements regardless if the string is an id, a class, or a tag as long as the element is existing in the webpage
*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// addEventListener
/*
	event - actions that the user is doing in our webpage (scroll, click, hover, keypress/type)

	addEventListener - function that lets the webpage to listen to the events performed by the user

	- take two arguments
		1. string - the event to which the HTML element will listen, these are predetermined
		2. function - executed by the element once the event (first argument) is triggered
*/

/*txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value
});

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);

});*/

/*
	//MINI ACTIVITY

	Make another keyup event where in the span element will record the last name in the forms and send the output in the batch google meet
*/

/*txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtLastName.value
});*/



// ACTIVITY S47

document.querySelector("#txt-first-name").onkeydown = function() {myFunction()};
document.querySelector("#txt-last-name").onkeyup = function() {myFunction()};

function myFunction() {
	document.querySelector("#span-full-name").innerHTML = `${txtFirstName.value} ${txtLastName.value}
		`;
	document.querySelector("#span-full-name").innerHTML = `${txtFirstName.value} ${txtLastName.value}
		`;
}
